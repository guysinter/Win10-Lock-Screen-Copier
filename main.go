package main

import (
"fmt"
_ "image/jpeg"
"io/ioutil"
"os"
"path/filepath"
	"log"
	"io"
	"image"
	"encoding/json"
)

const json_file string =  "./WinLockPicsCopierConf.json"

var configuration struct {
	ScanDir string `json:"scan_dir"`
	TargetDir  string `json:"target_dir"`
}
func main() {
	loadFromJson();
	fmt.Println(configuration.TargetDir)
	fmt.Println(configuration.ScanDir)
	filesToCopy := make([]os.FileInfo, 0)
	files, _ := ioutil.ReadDir(configuration.ScanDir)
	for _, file := range files {
		reader, err := os.Open(filepath.Join(configuration.ScanDir, file.Name()))
		if err != nil {
			fmt.Fprintf(os.Stderr, "Eror readin file %s: %v\n", file.Name(), err)
			log.Fatal(err)
		}
		defer reader.Close()
		im, _, err := image.DecodeConfig(reader)
		if im.Width == 1920 {
			filesToCopy = append(filesToCopy, file)
		}
	}
	for _, file := range filesToCopy {
		reader, err := os.Open(filepath.Join(configuration.ScanDir, file.Name()))
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error reading file %s: %v\n", file.Name(), err)
			log.Fatal(err)
		}
		defer reader.Close()
		fileDest := filepath.Join(configuration.TargetDir, file.Name())
		fileDest = fileDest + ".jpg"
		to, err := os.OpenFile(fileDest, os.O_RDWR|os.O_CREATE, 0777)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error creating  file %s: %v\n", file.Name(), err)
			log.Fatal(err)
		}
		defer to.Close()

		_, err = io.Copy(to, reader)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error copying  file %s: %v\n", file.Name(), err)
			log.Fatal(err)
		}
	}

}

func loadFromJson() {
	file, err := os.Open(json_file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %v\n", file.Name(), err)
		log.Fatal(err)
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&configuration)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		log.Fatal(err)
	}
}
